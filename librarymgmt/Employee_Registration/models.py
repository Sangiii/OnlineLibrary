from django.conf import settings
from django.db import models
from django.urls import reverse

from rest_framework.reverse import reverse as api_reverse

from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator


#django hosts --> subdomain for reverse
# class Employeename(models.Model):
#     employeename = models.CharField(max_length=50, null=True, blank=True)
    

#     def __str__(self):
#         return self.employeename

class EmployeeDetail(models.Model):
    # pk aka id --> numbers
    #user        = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE) 
    # Employeeid   = models.IntegerField(primary_key=True)

    employeename = models.CharField(max_length=120, null=True, blank=True)
    # phone_regex = RegexValidator(regex=r'^\+?1?\d{9,10}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    contact = models.CharField(max_length=10, validators=[RegexValidator(r'\d{10,10}','Number must be 10 digits','Invalid number')])
    # contact = models.CharField(validators=[phone_regex], max_length=10, blank=True)
    email = models.EmailField(max_length=254, blank=False, unique=True, validators=[validate_email])
    DEPARTMENT_CHOICES = (
        ('Sales & marketing','Sales & marketing'),
        ('Webapp Associate','Webapp Associate'),
        ('Webapp Architect','Webapp Architect'),
        ('Webapp Engineer','Webapp Engineer'),
        ('CMS & Ecomm','CMS & Ecomm'),
        ('System Admin','System Admin'),
        ('UI/UX','UI/UX'),
        ('Quality Analyst','Quality Analyst'),
        ('HR','HR'),
    )
    # Types       = models.CharField(max_length = 100, choices = DEPARTMENT_CHOICES)
    department   = models.CharField(max_length=100, choices = DEPARTMENT_CHOICES)
    # timestamp   = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.employeename
    ''''def custom_validate_email(value):
        if (custom_check):
            raise ValidationError('Email format is incorrect')'''


class EmployeeRenewal(models.Model):
    # pk aka id --> numbers
    #user        = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE) 
    Employeeid   = models.CharField(max_length=120, primary_key=True)
    employeename = models.ForeignKey(EmployeeDetail, related_name="custom_eename_profile", null=True, on_delete=models.CASCADE)
    # contact      = models.CharField(max_length=120, null=True, blank=True)
    # email        = models.CharField(max_length=120, null=True, blank=True)
    # timestamp   = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.Employeeid
    




'''@property
    def owner(self):
        return self.user

    # def get_absolute_url(self):
    #     return reverse("api-postings:post-rud", kwargs={'pk': self.pk}) '/api/postings/1/'
    
    def get_api_url(self, request=None):
        return api_reverse("postings:post-rud", kwargs={'pk': self.pk}, request=request)
'''