from django.contrib import admin


from .models import EmployeeRenewal, EmployeeDetail

class EmployeeDetailAdmin(admin.ModelAdmin):
	list_display = ('employeename', 'contact', 'email', 'department')

admin.site.register(EmployeeDetail, EmployeeDetailAdmin)

class EmployeeRenewalAdmin(admin.ModelAdmin):
	list_display = ( 'Employeeid', 'employeename')

admin.site.register(EmployeeRenewal, EmployeeRenewalAdmin)