from django.conf import settings
from django.db import models
from django.urls import reverse
from library_management.models import BookDetail, BookRegistration
from Employee_Registration.models import EmployeeRenewal, EmployeeDetail
from rest_framework.reverse import reverse as api_reverse
from django import forms

# django hosts --> subdomain for reverse
# class Employeename(models.Model):
#     employeename = models.CharField(max_length=50, null=True, blank=True)
    
#     def __str__(self):
#         return self.employeename

# class Bookname(models.Model):
#     bookname = models.CharField(max_length=50, null=True, blank=True)

#     def __str__(self):
#         return self.bookname

class Assignbook(models.Model):
    # pk aka id --> numbers
    Employeeid   = models.ForeignKey(EmployeeRenewal, related_name="custom_Employeeid_profile", on_delete=models.CASCADE, blank=True)
    employeename = models.ForeignKey(EmployeeDetail, related_name="custom_Eename_profile", on_delete=models.CASCADE, blank=True)
    BookName     = models.ForeignKey(BookDetail, null=True, on_delete=models.CASCADE, blank=True)
    BookId       = models.ForeignKey(BookRegistration, related_name="custom_BookId_profile", on_delete=models.CASCADE, blank=True)
    issue        = models.DateField()
    renew        = models.DateField()

    '''def clean(self):
        issue = cleaned_data.get("start_date")
        renew = cleaned_data.get("end_date")
        if renew < issue:
            msg = u"renew date is before issue date."
            self._errors["renew"] = self.error_class([msg])
    
    #  def __str__(self):
    #     return self.BookName '''

class Bookdetails(models.Model):
    employeename       = models.CharField(max_length=50, null=True, blank=True)
    assignbook         = models.ManyToManyField(BookDetail)

    # def __str__(self):
    #     return str(self.BookName)

   # @property
    #def owner(self):
     #   return self.types

    # def get_absolute_url(self):
    #     return reverse("api-postings:post-rud", kwargs={'pk': self.pk}) '/api/postings/1/'
    
    #def get_api_url(self, request=None):
     #   return api_reverse("api-postings:post-rud", kwargs={'pk': self.pk}, request=request)